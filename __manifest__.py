# -*- coding: utf-8 -*-

{
    'name': 'DW Picking Barcode',
    'version': '12.0.1.0.1',
    'summary': 'Barcode Support in Stock Picking.',
    'author': 'Deniz Yıldız',
    'maintainer': 'Deniz Yıldız',
    'depends': ['stock', 'purchase', 'barcodes'],
    'category': 'Inventory',
    'demo': [],
    'data': [
        'security/ir.model.access.csv',
        'views/stock_picking.xml',
        'views/stock_move_p_order_rel.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
    'qweb': [],
    'license': 'AGPL-3',
}
