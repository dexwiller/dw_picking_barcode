# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class PurchaseOrderLine(models.Model):
    _inherit = 'purchase.order.line'
    _name = 'purchase.order.line'

    remaining_unmatched_qty = fields.Float(string='Remained Unmatched Qty')

    @api.model
    def create(self, values):
        if values.get('product_qty'):
            values.update({'remaining_unmatched_qty': values.get('product_qty')})

        return super(PurchaseOrderLine, self).create(values)

    def write(self, values):
        for s in self:
            if values.get('product_qty'):
                diff = values.get('product_qty') - s.product_qty
                s.remaining_unmatched_qty = s.remaining_unmatched_qty + diff

        return super(PurchaseOrderLine, self).write(values)
