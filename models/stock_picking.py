# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class StockPicking(models.Model):
    _inherit = ['stock.picking', 'barcodes.barcode_events_mixin']
    _name = 'stock.picking'

    barcode = fields.Char('Barcode')
    message_type = fields.Selection([
        ('info', 'Barcode read with additional info'),
        ('not_found', 'No barcode found'),
        ('more_match', 'More than one matches found'),
        ('success', 'Barcode read correctly'),
    ], readonly=True)
    message = fields.Char(readonly=True)
    move_purchase_rel = fields.Many2many('stock.move.purchase.order.line.rel',
                                         relation='stock_picking_move_purchase_line_rel',
                                         column2='picking_id', column1='rel_id', string='Move and Purchase')

    picking_type_code = fields.Selection(related='picking_type_id.code')

    @api.onchange('move_ids_without_package')
    def onchange_move_ids_without_package(self):
        if self.picking_type_id.code == 'incoming':
            self.match_purchase()

    @api.model
    def create(self, values):
        res = super(StockPicking, self).create(values)
        for move in res.move_ids_without_package:
            move.mapped('move_line_ids').write({'picking_id': move.picking_id.id})

        return res

    def _set_messagge_info(self, message_type, message):
        """
        Set message type and message description.
        For manual entry mode barcode is not set so is not displayed
        """
        self.message_type = message_type
        if self.barcode:
            self.message = _('Barcode: %s (%s)') % (self.barcode, message)
        else:
            self.message = '%s' % message

    def create_move_line(self, lot_id, stock_move):

        move_line_obj = self.env.get('stock.move.line')
        move_line_vals = stock_move._prepare_move_line_vals()
        move_line_vals.update({
            'lot_id': lot_id.id,
            'lot_name': lot_id.name,
            'product_uom_qty': 1,
            'qty_done': 1,
            'picking_id': self.id
        })
        move_line = move_line_obj.create(move_line_vals)
        return move_line

    def set_move_line_reserve(self, move_id, minus=False, move_line=True):

        if move_line:
            for move_line in move_id.move_line_ids:
                if minus:
                    move_line.write({'product_uom_qty': move_line.product_uom_qty - 1})
                else:
                    move_line.write({'product_uom_qty': move_line.product_uom_qty + 1})

        else:
            if minus:
                move_id.write({'product_uom_qty': move_id.product_uom_qty - 1})
            else:
                move_id.write({'product_uom_qty': move_id.product_uom_qty + 1})

        move_id._action_assign()

    def create_rel(self, move_line, po):
        first_rels = self.move_purchase_rel.ids
        values = {
            'picking_id': False,
            'stock_move_id': move_line.id,
            'purchase_order_line_id': po.id,
            'stock_move_done_qty': 0
        }

        rel = self.env.get('stock.move.purchase.order.line.rel').sudo()
        rel = rel.create(values)
        first_rels.append(rel.id)

        self.move_purchase_rel = [(6, 0, first_rels)]
        return rel

    def match_qty(self, stock_qty, rel=False, po=False, move_line=False):
        remaining = 0
        if rel:
            if stock_qty >= rel.available_qty:  # move fully cover the po
                remaining = stock_qty - rel.available_qty
                new_qty = rel.purchase_order_line_qty
            else:  # not enough.
                new_qty = rel.stock_move_done_qty + stock_qty
            move_line = rel.stock_move_id
            po = rel.purchase_order_line_id

        else:
            # at least one purchase should be added to rel in this case.
            rel = self.create_rel(move_line, po)
            if stock_qty >= po.remaining_unmatched_qty:  # move fully cover the po
                remaining = stock_qty - po.remaining_unmatched_qty
                new_qty = po.remaining_unmatched_qty
            else:  # not enough.
                new_qty = rel.stock_move_done_qty + stock_qty

        rel.sudo().write({'stock_move_done_qty': new_qty})

        move_line.write({'matched_qty': move_line.matched_qty + (stock_qty - remaining)})
        po.write({'remaining_unmatched_qty': po.remaining_unmatched_qty - (stock_qty - remaining)})

        return remaining

    def match_purchase(self):
        for stock_move_id in self.move_ids_without_package:  # we has to sync all the lines in every action cos of odoo's weird onchange behaviour.
            # this can be problematic.
            # if there is matched po for this move search for those, if not, create new one from available purchases.

            stock_qty = stock_move_id.quantity_done - stock_move_id.matched_qty  # remaining qty to cover po's
            if stock_qty > 0:
                existing_rels = self.move_purchase_rel.filtered(
                    lambda x: x.remaining_unmatched_qty > 0 and x.stock_move_id.id == stock_move_id.id).sorted(
                    lambda x: x.id)  # fifo
                if existing_rels:
                    for rel in existing_rels:
                        stock_qty = self.match_qty(stock_qty, rel=rel)
                        # prevent double execution.
                        if stock_qty == 0:
                            break

            # if there is still stock_qty, search for another purchases.
            if stock_qty > 0:
                po = self.env.get('purchase.order.line').search([('partner_id', '=', self.partner_id.id),
                                                                 ('product_id', '=', stock_move_id.product_id.id),
                                                                 ('remaining_unmatched_qty', '>', 0),
                                                                 ('state', 'in',
                                                                  ('draft', 'sent', 'purchase', 'to approve'))],
                                                                order='id asc')  # fifo
                for p in po:
                    stock_qty = self.match_qty(stock_qty, po=p, move_line=stock_move_id)
                    if stock_qty == 0:
                        break

    def on_barcode_scanned(self, barcode):
        self.barcode = barcode
        lot_id = False
        match = False
        product_obj = self.env['product.product']
        lot_obj = self.env['stock.production.lot']

        product_id = product_obj.search([('barcode', '=', barcode)])
        if barcode:
            if not product_id:
                # search for lots
                lot_id = lot_obj.search([('name', '=', barcode)])
                if not lot_id:
                    self._set_messagge_info('not_found', _('Barcode not found in neither products, serials nor lots.'))
                    return
                product_id = lot_id.product_id
                if product_id.tracking not in ['lot', 'serial']:
                    self._set_messagge_info('not_found',
                                            _(
                                                'Barcode found in lots or serials but Product Tracking did not set as "Track by Lots" or "Serial Numbers"'))
                    return

            if self.move_ids_without_package:
                for line in self.move_ids_without_package:
                    if line.product_id.id == product_id.id:
                        match = True
                        if line.created_from_barcode:
                            self.set_move_line_reserve(line, move_line=False)
                        if lot_id:
                            if not line.move_line_ids.filtered(lambda x: x.lot_id.name == barcode):
                                lot_used = False
                                for move_line_id in line.move_line_ids:
                                    if not move_line_id.lot_id:
                                        lot_used = True
                                        move_line_id.write({'lot_id': lot_id.id,
                                                            'qty_done': 1,
                                                            'state': 'done'})

                                        break
                                if not lot_used:
                                    line.move_line_ids = [(4,
                                                           self.create_move_line(lot_id, line).id
                                                           )]
                                
                                self._set_messagge_info('success', _('Lot or Serial Added and Done Quantity Up By One'))
                            else:
                                if line.created_from_barcode:
                                    self.set_move_line_reserve(line, minus=True, move_line=False)
                                self._set_messagge_info('more_match',
                                                        _('Lot or Serial Barcode Already Added'))

                        else:
                            if self.picking_type_id.code == 'incoming':
                                line.write({'quantity_done': line.quantity_done + 1})
                            self.set_move_line_reserve(line)
                            self._set_messagge_info('success', _('Done Quantity Up By One'))

            if not match:
                first_moves = self.move_ids_without_package.ids
                source = 'product'
                if lot_id:
                    source = 'lot'
                move_vals = {
                    'name': product_id.display_name,
                    'product_id': product_id.id,
                    'product_uom_qty': 1,
                    'quantity_done': self.picking_type_id.code == 'incoming' and 1 or 0,
                    'product_uom': product_id.uom_id.id,
                    'location_id': self.location_id.id,
                    'location_dest_id': self.location_dest_id.id,
                    'picking_type_id': self.picking_type_id.id,
                    'barcode': product_id.barcode,
                    'created_from_barcode': source,
                    'date_expected': self.scheduled_date or fields.Datetime.now(),
                    'state': 'draft',
                    'picking_id': self.id
                }
                move_id = self.env.get('stock.move').create(move_vals)
                first_moves.append(move_id.id)

                # noinspection PyAttributeOutsideInit
                self.move_ids_without_package = [(6, 0, first_moves)]
                if lot_id:
                    move_id.move_line_ids = [(4, self.create_move_line(lot_id, move_id).id)]
                else:
                    self.set_move_line_reserve(move_id)
                self._set_messagge_info('success', _('New Product Move Created'))

    def action_see_purchase_move_rel(self):
        action = {
            'name': _('Purchase and Moves'),
            'type': 'ir.actions.act_window',
            'res_model': 'stock.move.purchase.order.line.rel',
            'view_mode': 'tree',
            'view_type': 'form',
            'target': 'new',
            'domain': [('picking_id', '=', self.id)]
        }

        return action
