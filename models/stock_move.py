# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class StockMove(models.Model):
    _inherit = 'stock.move'
    _name = 'stock.move'

    barcode = fields.Char(string='Barcode')
    created_from_barcode = fields.Selection(
        selection=[('lot', 'Created By Lot Barcode'), ('product', 'Created By Product Barcode')])
    matched_qty = fields.Float(string='Remained Unmatched Qty')

    # @api.onchange('quantity_done')
    # def quantity_done_onchange(self):
    #     print (self._origin.picking_id.id)
    #     d = self.env.get('stock.move').browse([self._origin.id])
    #     print (d.picking_id)
    #     self.picking_id.match_purchase(self._origin, self.quantity_done)

    def _quantity_done_set(self):
        quantity_done = self[0].quantity_done  # any call to create will invalidate `move.quantity_done`

        for move in self:
            move_lines = move._get_move_lines()
            if not move_lines:
                if quantity_done and not move.created_from_barcode == 'lot':
                    # do not impact reservation here
                    move_line = self.env['stock.move.line'].create \
                        (dict(move._prepare_move_line_vals(), qty_done=quantity_done))
                    move.write({'move_line_ids': [(4, move_line.id)]})
            elif len(move_lines) == 1:
                move_lines[0].write({'qty_done': quantity_done})
            # else:
            #     raise UserError(
            #         _("Cannot set the done quantity from this stock move, work directly with the move lines."))
