# -*- coding: utf-8 -*-

from odoo import fields, models, api, _


class StockMovePurchaseOrderLine(models.Model):
    _name = 'stock.move.purchase.order.line.rel'

    picking_id = fields.Many2many('stock.picking', relation='stock_picking_move_purchase_line_rel',
                                  column1='picking_id', column2='rel_id', string='Picking', default=False)
    stock_move_id = fields.Many2one('stock.move', string='Stock Move', required=True)
    purchase_order_line_id = fields.Many2one('purchase.order.line', string='Purchase Order Line', required=True)
    purchase_id = fields.Many2one(related='purchase_order_line_id.order_id',string='Purchase Order')

    stock_move_done_qty = fields.Float(string='Quantity Done', default=0)
    purchase_order_line_qty = fields.Float(related='purchase_order_line_id.product_uom_qty', string='First Quantity Demanded')
    remaining_unmatched_qty = fields.Float(string='Remained Unmatched Qty', related='purchase_order_line_id.remaining_unmatched_qty')

    available_qty = fields.Float(compute='_get_available_qty', string='Available Quantity', store=True,
                                 default=lambda self: self.purchase_order_line_qty)
    product_id = fields.Many2one(related='stock_move_id.product_id', string='Product')
    product_uom = fields.Many2one(related='stock_move_id.product_uom', string='Product Uom')

    @api.depends('stock_move_done_qty')
    def _get_available_qty(self):
        for s in self:
            if s.stock_move_done_qty > 0:
                available_qty = s.purchase_order_line_qty - s.stock_move_done_qty
                if available_qty < 0:
                    available_qty = 0
                s.available_qty = available_qty